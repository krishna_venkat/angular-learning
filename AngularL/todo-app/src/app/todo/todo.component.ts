import { Component } from '@angular/core';
import { TodoService } from '../todo.service';
import { Todo } from '../todo';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css'],
  providers: [TodoService]
})

export class TodoComponent {
  todoText: string;

  constructor(private todoService: TodoService) {
    this.todoText = '';
  }

  addTodo(): void {
    this.todoService.addTodo(this.todoText);
    this.todoText = '';
  }

  todos :Todo[] = this.todoService.getTodos();

  private removeTodo(id :number): void {
    this.todoService.removeTodo(id);
  }
}

