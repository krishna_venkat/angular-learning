
function check_pass() {
  var pass = document.getElementById("pass").value;
  var confirm_pass = document.getElementById("confirm_pass").value;

  if (pass === confirm_pass) {
    document.getElementById("warn").innerHTML = "";
    return true;
  }
  else {
    document.getElementById("warn").innerHTML = "Passwords does not match";
    document.getElementById("warn").style.color = "darkred";
    return false;
  }
}

user_list = localStorage.getItem("user_list");
var users = user_list == null ? [] : JSON.parse(localStorage.getItem("user_list"));

function register() {
  var name = document.getElementById("name").value;
  var pass = document.getElementById("pass").value;

  document.getElementById("regForm").reset();

  for (var user = 0; user < users.length; user++) {
    if (users[user].name === name) {
      alert("User name already exists !");
      return;
    }
  };

  users.push({ name: name, pass: pass });

  localStorage.setItem('user_list', JSON.stringify(users));
}

function loginPage() {
  location.href = "Login.html";
}