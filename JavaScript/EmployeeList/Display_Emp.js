function deleteEmployee(employee_id) {
  var list = JSON.parse(localStorage.getItem("employee_list"));
  list.splice(employee_id);
  var row = document.getElementById(employee_id);
  row.parentNode.parentNode.parentNode.removeChild(row.parentNode.parentNode);
}

function createTable(tableData) {
  var imageID = 0;
  var table = document.createElement('table');
  table.id = "table";
  var tableBody = document.createElement('tbody');

  var headingData = ['First Name', 'Last Name', 'Date of birth', 'Address', 'Phone', 'Email', 'Zip', 'Edit', 'Delete'];
  var hrow = document.createElement('tr');
  headingData.forEach(function (cellData) {
    var cell = document.createElement('th');
    cell.appendChild(document.createTextNode(cellData));
    hrow.appendChild(cell);
  });

  tableBody.appendChild(hrow);
  tableData.forEach(function (rowData) {
    var row = document.createElement('tr');

    rowData.forEach(function (cellData) {
      var cell = document.createElement('td');
      cell.appendChild(document.createTextNode(cellData));
      row.appendChild(cell);
    });

    var cell = document.createElement('td');
    var editImage = document.createElement("img");
    editImage.setAttribute("src", "Images/edit.png");
    editImage.setAttribute("id", imageID);
    cell.appendChild(editImage);
    row.appendChild(cell);

    var cell = document.createElement('td');
    var deleteImg = document.createElement("img");
    deleteImg.setAttribute("src", "Images/delete.png");
    deleteImg.setAttribute("onclick", "deleteEmployee(this.id);");
    deleteImg.setAttribute("id", imageID);
    cell.appendChild(deleteImg);
    row.appendChild(cell);

    tableBody.appendChild(row);
    imageID++;
  });

  table.appendChild(tableBody);
  document.body.appendChild(table);
}

var employeeList1 = JSON.parse(localStorage.getItem("employee_list"));

var employeeList = [];
if (employeeList1 != null) {
  employeeList1.forEach(function (emp) {
    employeeList.push((Object.values(emp)));
  });
}

createTable(employeeList);
