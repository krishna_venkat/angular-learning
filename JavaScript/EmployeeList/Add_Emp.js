function isformValid() {
  var errorMessages = document.getElementsByTagName("span");
  for (var element = 0; element < errorMessages.length; element++) {
    if (errorMessages[element].innerHTML != "") {
      return false;
    }
  }
  return true;
}

function isAlphabet(element, event) {
  var isTrue = ((event.which >= 58 && event.which < 127) || (event.which < 48))
  if (isTrue) {
    document.getElementById(element.id + 'Error').innerHTML = "";
    }
  else {
    document.getElementById(element.id + 'Error').innerHTML = "Only alphabets are allowed";
  
  }
  return isTrue;
}

function isNumber(element, event) {
  var isTrue = ((event.which >= 48 && event.which < 58) || (event.which == 8))
  if (isTrue) {
    document.getElementById(element.id + 'Error').innerHTML = "";
    }
  else {
    document.getElementById(element.id + 'Error').innerHTML = "Only numbers are allowed";
  
  }
  return isTrue;
}

function checkDate() {
  date = document.getElementById("dob").value;
  var enteredDate = new Date(date);
  var today = new Date();

  if (enteredDate < today) {
    document.getElementById('dobError').innerHTML = "";
    }
  else {
    document.getElementById('dobError').innerHTML = "Invalid date";
  
  }
}

var employeeList_1 = localStorage.getItem("employee_list");
var employeeList = employeeList_1 == null ? [] : JSON.parse(employeeList_1);

function registerEmp() {
  var firstName = document.getElementById("Fname").value;
  var lastName = document.getElementById("Lname").value;
  var dob = document.getElementById("dob").value;
  var address = document.getElementById("address").value;
  var phone = document.getElementById("phone").value;
  var email = document.getElementById("email").value;
  var zip = document.getElementById("zip").value;

  document.getElementById("regForm").reset();

  employeeList.push({
    firstName: firstName,
    lastName: lastName,
    dob: dob,
    address: address,
    phone: phone,
    email: email,
    zip: zip
  });

  localStorage.setItem('employee_list', JSON.stringify(employeeList));
}

function viewlist() {
  location.href = "Display_Emp.html";
}